# Despliegue de Grafana en Kubernetes

Vamos a desplegar un Grafana usando los manifiestos vistos en clase, comentando las partes del codigo .yaml detallando las funciones de las variables utilizadas y el paso a paso de como desplegarlos con los comandos propios de minikube.

## Manifiestos

Se listan los manifiestos utilizados para desplegar un Grafana:

### ConfigMap

Nos permite almacenar configuraciones de Grafana fuera del contenedor en si, permite actualizar la misma sin volver a armar todo de vuelta. También permite conectar con otras aplicaciones, gestionar las fuentes de donde se visualizan los datos en los tableros, etc. El código que use para el mismo es el siguiente:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: grafana-config
  namespace: istea
data:
  grafana.ini: |
    [paths]
    data = /var/lib/grafana/data
    logs = /var/log/grafana
    plugins = /var/lib/grafana/plugins
    provisioning = /etc/grafana/provisioning

    [server]
    protocol = http
    http_addr =
    http_port = 3000
    domain = localhost
    root_url = %(protocol)s://%(domain)s:%(http_port)s/
    serve_from_sub_path = false

    [database]
    type = sqlite3
    path = grafana.db
    cache_mode = private

    [session]
    provider = file
    provider_config = sessions
    cookie_name = grafana_sess
    cookie_secure = false

    [analytics]
    check_for_updates = true

    [security]
    admin_user = admin
    admin_password = admin
    secret_key = SW2YcwTIb9zpOOhoPsMm

    [snapshots]
    external_enabled = true
    snapshot_url = https://snapshots-origin.raintank.io

    [dashboards.json]
    enabled = true
    path = /var/lib/grafana/dashboards

    [log]
    mode = console
    level = info

    [grafana_net]
    url = https://grafana.net
```

### Deployment
En este archivo definimos la imagen del servicio que vamos a utilizar, en este caso un Grafana, tambien se marcan la cantidad de replicas del pod que queremos se creen, el namespace donde se desplegaran, el nombre que se le asigna, donde se guardaran las configuraciones del servicio (dentro del pod) y la asociación con el pvc:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: grafana
  namespace: istea
spec:
  replicas: 2
  selector:
    matchLabels:
      app: grafana
  template:
    metadata:
      labels:
        app: grafana
    spec:
      containers:
      - name: grafana
        image: grafana/grafana:latest
        ports:
        - containerPort: 3000
        volumeMounts:
        - name: grafana-storage
          mountPath: /var/lib/grafana
        - name: grafana-config
          mountPath: /etc/grafana/grafana.ini
          subPath: grafana.ini
      volumes:
      - name: grafana-storage
        persistentVolumeClaim:
          claimName: grafana-pvc
      - name: grafana-config
        configMap:
          name: grafana-config
```

### PersistentVolumeClaim
PersistentVolumeClaim es un recurso que se utiliza para solicitar almacenamiento persistente de un  dentro de un cluster. Le hacen una solicitud de almacenamiento por parte de una aplicacion o pod, especificando cuanto almacenamiento necesita y que caracteristicas debe tener.

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: grafana-pvc
  namespace: istea
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 10Gi
```

### Service
El service permite exponer una app (Grafana) o pods hacia la red, podemos ver que con el selector elegimos todos los pods en el namespace que tengan un grafana, mediante el service podemos recibir el trafico necesario.

```yaml
apiVersion: v1
kind: Service
metadata:
  name: grafana
  namespace: istea
spec:
  selector:
    app: grafana
  ports:
    - protocol: TCP
      port: 80
      targetPort: 3000
  type: NodePort
```

### Ingress
El ingress permite gestionar el acceso a la aplicacion mediante un nombre de dominio, que en nuestro caso, tendremos que modificar en el archivo host de nuestro sistema. Cabe aclarar que precisamos un Ingress Controller en nuestro cluster para gestionar las reglas de acceso.

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: grafana-ingress
  namespace: istea
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - host: grafana.example.com
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: grafana
            port:
              number: 80
  tls:
  - hosts:
  # Este host se debe colocar en el archivo hosts del so.
    - grafana.com
    secretName: grafana-tls
```

### PersistentVolume

Es un recurso de almacenamiento que proporciona persistencia a las aplicaciones desplegadas en el cluster. Permite almacenar y recuperar datos de manera persistente, incluso si los pods que las utilizan son eliminados o reiniciados. El código que use para el mismo es el siguiente:

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: grafana-pv
  namespace: istea
spec:
  capacity:
    storage: 10Gi
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  storageClassName: manual
  hostPath:
    path: /mnt/data/grafana
```

## Pasos para Desplegar

**Iniciar Minikube**:
minikube start

**Crear el name space**:
kubectl create ns istea

**Desplegar el PV**:
kubectl apply -f pv.yaml

**Desplegar el PVC**:
kubectl apply -f pvc.yaml

**Desplegar el Configmap**:
kubectl apply -f configmap.yaml

**Desplegar el Deployment**:
kubectl apply -f deployment.yaml

**Desplegar el Service**:
kubectl apply -f service.yaml

**Desplegar el Ingress**:
kubectl apply -f ingress.yaml

**Visualizar pods**:
kubectl get pods -n istea

**Visualizar logs de un pod**:
kubectl logs grafana-c857fdd94-4d7ck -n istea

**Realizar un tunnel para ver en nuestro localhost el sitio de grafana**:
 minikube service grafana -n istea 
